# shekan

shekan is a shell script can modified `resolv.conf` and add [shecan](https://shecan.ir/) and [begzar](https://begzar.ir/)  dns


## Instalation

```bash
git clone https://codeberg.org/ssmns/shekan.git
cd shekan
chmod +x shekan
sudo cp shekan /usr/bin/shekan
```

## Use

print help
```bash 
sudo shekan help
```

set dns
```bash
Shekan Ver 0.19 

 how to use shekan 

   Options:

   Set DNS:
 - start: set shecan dns
 - start shecan: set shecan dns
 - start begzar: set begzar dns
 - start google: set google dns
 - start 403: set 403 dns
 - start hostiran: set hostiran dns
 - stop: revoke shekan's dns

   Set pip mirror:
 - pip : set tuna  university mirror fpr pip
 - pip tuna : set tuna  university mirror fpr pip
 - pip aliyun : set aliyun  mirror fpr pip
 - pip ustc : set ustc  mirror fpr pip
 - pip huawei : set huawei  mirror fpr pip
 - pip douban : set douban  mirror fpr pip
 - pip sdutlinux : set sdutlinux  mirror fpr pip
 - pip hustunique : set hustunique  mirror fpr pip
 - pip [stop | disable | reset]: revoke pip mirror to defaut

 - help: print this text
 - status: tail '/etc/resolv.conf'


```

revoke dns
```bash
sudo shekan stop
```

print current `resolv.conf` content
```bash 
sudo shekan status
```